PKG:=CAMB

SD:=$(SRCROOT)/$(PKG)
OD:=$(BLDROOT)/$(PKG)

FULL_INCLUDE_F+= $(F_MODPATH)$(OD)

LIB_$(PKG):=$(LIBDIR)/libcamb.a
ALLOBJ:=constants.o reionization.o subroutines.o inifile.o power_tilt.o recfast.o modules.o bessels.o equations.o lensing.o cmbmain.o camb.o writefits.o inidriver.o halofit.o utils.o reionization.o SeparableBispectrum.o
ALLOBJ:=$(ALLOBJ:%=$(OD)/%)
OBJ_$(PKG):=$(ALLOBJ)
MODULES:=camb_main
MODULES:=$(MODULES:%=$(OD)/%.$(MOD))

ODEP:=$(OBJ_Modules) $(OBJ_portability)
$(LIB_$(PKG)): $(ALLOBJ)
$(ALLOBJ): $(ODEP) | $(OD)_mkdir
f90prep: | $(OD)_mkdir

$(OD)/subroutines.o: $(OD)/utils.o
$(OD)/power_tilt.o: $(OD)/subroutines.o $(OD)/inifile.o
$(OD)/recfast.o: $(OD)/inifile.o $(OD)/constants.o $(OD)/subroutines.o
$(OD)/modules.o: $(OD)/constants.o $(OD)/subroutines.o $(OD)/recfast.o $(OD)/power_tilt.o $(OD)/inifile.o $(OD)/reionization.o
$(OD)/bessels.o: $(OD)/subroutines.o $(OD)/modules.o
$(OD)/SeparableBispectrum.o: $(OD)/bessels.o $(OD)/inifile.o $(OD)/modules.o $(OD)/lensing.o $(OD)/power_tilt.o
$(OD)/equations.o: $(OD)/subroutines.o $(OD)/modules.o
$(OD)/lensing.o: $(OD)/subroutines.o $(OD)/modules.o $(OD)/utils.o
$(OD)/cmbmain.o: $(OD)/subroutines.o $(OD)/bessels.o $(OD)/modules.o $(OD)/equations.o $(OD)/halofit.o $(OD)/SeparableBispectrum.o
$(OD)/camb.o: $(OD)/constants.o $(OD)/subroutines.o $(OD)/modules.o $(OD)/cmbmain.o $(OD)/lensing.o $(OD)/SeparableBispectrum.o
$(OD)/writefits.o: $(OD)/camb.o
$(OD)/inidriver.o: $(OD)/inifile.o $(OD)/camb.o $(OD)/modules.o $(OD)/SeparableBispectrum.o
$(OD)/halofit.o: $(OD)/modules.o $(OD)/equations.o
$(OD)/reionization.o: $(OD)/constants.o $(OD)/subroutines.o $(OD)/recfast.o $(OD)/inifile.o

all_lib+=$(LIB_$(PKG))
all_mod+=$(MODULES)
