PKG:=allmod

SD:=$(SRCROOT)/$(PKG)
OD:=$(BLDROOT)/$(PKG)

CXXBIN:=LevelS_cxx
F90BIN:=LevelS_f90
CXXOBJ:=$(CXXBIN:%=%.o)
F90OBJ:=$(F90BIN:%=%.o)
CXXOBJ:=$(CXXOBJ:%=$(OD)/%)
F90OBJ:=$(F90OBJ:%=$(OD)/%)

CXXODEP:=$(HDR_cxxsupport)
ifeq ($(MIXBIN_SUPPORTED),yes)
CXXBDEP:=$(OBJ_LFI-specific) $(OBJ_cxxtools) $(OBJ_multimod) $(OBJ_pntsrcconvolver) $(OBJ_cxxmod) $(OBJ_zlelib) $(OBJ_mod) $(OBJ_portability) $(OBJ_conviqt) $(OBJ_skymixer2) $(OBJ_Healpix_cxx) $(OBJ_cxxsupport) $(OBJ_libsharp) $(OBJ_libfftpack) $(OBJ_c_utils) $(LIB_libcfitsio)
else
CXXBDEP:=$(OBJ_LFI-specific) $(OBJ_cxxtools) $(OBJ_multimod) $(OBJ_pntsrcconvolver) $(OBJ_cxxmod) $(OBJ_conviqt) $(OBJ_skymixer2) $(OBJ_Healpix_cxx) $(OBJ_cxxsupport) $(OBJ_libsharp) $(OBJ_libfftpack) $(OBJ_c_utils) $(LIB_libcfitsio)
endif
F90ODEP:=$(OBJ_simmission3) $(OBJ_Beam) $(OBJ_CAMB) $(OBJ_Modules) $(OBJ_libsharp_f) $(OBJ_portability)
F90BDEP:=$(OBJ_simmission3) $(OBJ_Beam) $(OBJ_CAMB) $(OBJ_Modules) $(OBJ_libsharp_f) $(OBJ_portability) $(OBJ_libsharp) $(OBJ_libfftpack) $(OBJ_c_utils) $(LIB_libcfitsio)

$(CXXOBJ): $(CXXODEP) | $(OD)_mkdir
CXXBIN:=$(CXXBIN:%=$(BINDIR)/%)
$(CXXBIN): $(BINDIR)/% : $(OD)/%.o $(CXXBDEP)
$(F90OBJ): $(F90ODEP) | $(OD)_mkdir
F90BIN:=$(F90BIN:%=$(BINDIR)/%)
$(F90BIN): $(BINDIR)/% : $(OD)/%.o $(F90BDEP)

ifeq ($(MIXBIN_SUPPORTED),yes)
  all_mixbin+=$(CXXBIN)
else
  all_cxxbin+=$(CXXBIN)
endif
all_f90bin+=$(F90BIN)

levels_bin+=LevelS_cxx LevelS_f90
