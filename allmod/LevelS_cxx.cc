#include "codeRegistry.h"

using namespace std;

#define QUICKREG(name) int name (int, const char **); CodeRegistrator reg_##name(#name,name);
#define QUICKREGC(name) extern "C" int name (int, const char **); CodeRegistrator reg_##name(#name,name);

QUICKREGC(ffttest)
QUICKREGC(morton_test)
QUICKREGC(sharp_testsuite)
QUICKREGC(trigtest)

QUICKREG(syn_alm_cxx)
QUICKREG(alm2map_cxx)
QUICKREG(anafast_cxx)
QUICKREG(totalconvolve_cxx)
QUICKREG(alm2grid)
QUICKREG(calc_powspec)
QUICKREG(hotspots_cxx)
QUICKREG(map2tga)
QUICKREG(median_filter_cxx)
QUICKREG(mult_alm)
QUICKREG(smoothing_cxx)
QUICKREG(udgrade_cxx)
QUICKREG(udgrade_harmonic_cxx)
QUICKREG(conviqt_v3)
QUICKREG(conviqt_v4)
QUICKREG(multimod)
QUICKREG(skymixer3)
QUICKREG(almmixer)
QUICKREG(rotalm_cxx)
QUICKREG(addMaps)
QUICKREG(addTOI)
QUICKREG(beamsampler)
QUICKREG(ddlconvert)
QUICKREG(dmc_transfer)
QUICKREG(earl_importer)
QUICKREG(ephemeris_helper)
QUICKREG(focalplane)
QUICKREG(fpdbhelper)
QUICKREG(HPXconvert_cxx)
QUICKREG(makeMap)
QUICKREG(object_inspector)
QUICKREG(pntsrc2alm)
QUICKREG(pntsrc2map)
QUICKREG(pointing_errors)
QUICKREG(read_horizons)
QUICKREG(ringset2map)
QUICKREG(ringset2mapset)
QUICKREG(rotalm_phi)
QUICKREG(rotmap_cxx)
QUICKREG(sat2quat)
QUICKREG(sat_convert)
QUICKREG(text2powspec)
QUICKREG(ringset_compare)
QUICKREG(ahf2satpt)
QUICKREG(ls2lfitoi)
QUICKREG(quantum)
QUICKREG(hpxtest)
QUICKREG(compute_weights)
QUICKREG(needlet_tool)
QUICKREG(pntsrcmixer)

int main(int argc, const char **argv)
  {
  PLANCK_DIAGNOSIS_BEGIN
  CodeRegistry::execute(argc-1,&argv[1]);
  PLANCK_DIAGNOSIS_END
  }
