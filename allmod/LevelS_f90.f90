program LevelS_f90
use planck_config
use ls_misc_utils
use grasp2stokes_main
use crosspol_main
use beam2alm_main
use gaussbeampol_main
use stokes_extract_main
use camb_main
use simmission3_main
use simmission4_main
implicit none

character(len=filenamelen),dimension(:),allocatable :: argv
integer retval

argv=getcmdline()

call assert(size(argv)>0,"not enough command line arguments")

if (argv(1)=="camb") then
  retval = camb_module2(argv(2:))
else if (argv(1)=="grasp2stokes") then
  retval = grasp2stokes_module2(argv(2:))
else if (argv(1)=="crosspol") then
  retval = crosspol_module2(argv(2:))
else if (argv(1)=="beam2alm") then
  retval = beam2alm_module2(argv(2:))
else if (argv(1)=="gaussbeampol") then
  retval = gaussbeampol_module2(argv(2:))
else if (argv(1)=="stokes_extract") then
  retval = stokes_extract(argv(2:))
else if (argv(1)=="simmission3") then
  retval = simmission3_module2(argv(2:))
else if (argv(1)=="simmission4") then
  retval = simmission4_module2(argv(2:))
else
  call exit_with_status (1,"code not found")
endif

deallocate(argv)

if (retval/=0) call exit_with_status(retval,"error exit")

end program
