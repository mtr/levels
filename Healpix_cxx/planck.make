PKG:=Healpix_cxx

SD:=$(SRCROOT)/$(PKG)
OD:=$(BLDROOT)/$(PKG)

FULL_INCLUDE+= -I$(SD)

HDR_$(PKG):=$(SD)/*.h
LIB_$(PKG):=$(LIBDIR)/libhealpix_cxx.a
SPHERE_OBJ:=  powspec.o alm.o alm_powspec_tools.o
HEALPIX_OBJ:= healpix_tables.o healpix_base.o healpix_map.o alm_healpix_tools.o moc_query.o weight_utils.o
IO_OBJ:= alm_dmcio.o alm_fitsio.o powspec_dmcio.o powspec_fitsio.o healpix_data_io.o healpix_map_dmcio.o healpix_map_fitsio.o moc_fitsio.o
MOD_OBJ:= syn_alm_cxx_module.o anafast_cxx_module.o totalconvolve_cxx_module.o alm2map_cxx_module.o map2tga_module.o udgrade_cxx_module.o udgrade_harmonic_cxx_module.o smoothing_cxx_module.o hotspots_cxx_module.o alm2grid_module.o calc_powspec_module.o median_filter_cxx_module.o mult_alm_module.o rotalm_cxx.o hpxtest.o compute_weights_module.o needlet_tool_module.o
ALLOBJ:= $(SPHERE_OBJ) $(HEALPIX_OBJ) $(IO_OBJ) $(MOD_OBJ)
ALLOBJ:=$(ALLOBJ:%=$(OD)/%)
OBJ_$(PKG):=$(ALLOBJ)

ODEP:=$(HDR_$(PKG)) $(HDR_cxxsupport) $(HDR_libsharp) $(HDR_libfftpack) $(HDR_c_utils)

$(LIB_$(PKG)): $(ALLOBJ)

$(ALLOBJ): $(ODEP) | $(OD)_mkdir

all_hdr+=$(HDR_$(PKG))
all_lib+=$(LIB_$(PKG))
