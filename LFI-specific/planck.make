PKG:=LFI-specific

SD:=$(SRCROOT)/$(PKG)
OD:=$(BLDROOT)/$(PKG)

LIB_$(PKG):=$(LIBDIR)/libLFI-specific.a
ALLOBJ:= quantum.o ls2lfitoi.o ahf2satpt.o
ALLOBJ:=$(ALLOBJ:%=$(OD)/%)
OBJ_$(PKG):=$(ALLOBJ)

ODEP:=$(HDR_cxxsupport)

$(ALLOBJ): $(ODEP) | $(OD)_mkdir
$(LIB_$(PKG)): $(ALLOBJ)

all_lib+=$(LIB_$(PKG))
