/*
 *  This file is part of libcxxsupport.
 *
 *  libcxxsupport is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  libcxxsupport is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with libcxxsupport; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

/*
 *  libcxxsupport is being developed at the Max-Planck-Institut fuer Astrophysik
 *  and financially supported by the Deutsches Zentrum fuer Luft- und Raumfahrt
 *  (DLR).
 */

/*! \file codeRegistry.cc
 *  Copyright (C) 2015-2016 Max-Planck-Society
 *  Author: Martin Reinecke
 */

#include "codeRegistry.h"

using namespace std;

CodeRegistry::maptype & CodeRegistry::Dict()
  {
  static maptype dict;
  return dict;
  }

void CodeRegistry::printCodes()
  {
  cout << "Available codes:" << endl << endl;
  for (maptype::const_iterator iter = Dict().begin(); iter!=Dict().end(); ++iter)
    cout << "  " << iter->first << endl;
  cout << endl;
  }

int CodeRegistry::execute(int argc, const char **argv)
  {
  if (argc<=0) printCodes();
  planck_assert(argc>0,"argc==0!");
  maptype::const_iterator iter = Dict().find(argv[0]);
  planck_assert (iter!=Dict().end(),
    "codeRegistry::execute(): could not find code '"+string(argv[0])+"'");
  return Dict()[argv[0]](argc,argv);
  }
