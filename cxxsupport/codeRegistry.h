/*
 *  This file is part of libcxxsupport.
 *
 *  libcxxsupport is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  libcxxsupport is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with libcxxsupport; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

/*
 *  libcxxsupport is being developed at the Max-Planck-Institut fuer Astrophysik
 *  and financially supported by the Deutsches Zentrum fuer Luft- und Raumfahrt
 *  (DLR).
 */

/*! \file codeRegistry.h
 *  Copyright (C) 2015-2016 Max-Planck-Society
 *  Author: Martin Reinecke
 */

#ifndef PLANCK_CODEREGISTRY_H
#define PLANCK_CODEREGISTRY_H

#include <map>
#include <string>
#include "error_handling.h"

class CodeRegistry
  {
  public:
    typedef int (*mainfunc)(int, const char **);

  private:
    typedef std::map<std::string,mainfunc> maptype;
    static maptype &Dict();

  public:
    static void registerCode (const std::string &name, mainfunc func)
      { Dict()[name] = func; }

    static void printCodes();

    static int execute(int argc, const char **argv);
  };

class CodeRegistrator
  {
  public:
    CodeRegistrator(const std::string &name, CodeRegistry::mainfunc func)
      { CodeRegistry::registerCode(name, func); }
  };

#endif
