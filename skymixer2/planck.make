PKG:=skymixer2

SD:=$(SRCROOT)/$(PKG)
OD:=$(BLDROOT)/$(PKG)

HDR_$(PKG):=$(SD)/*.h
LIB_$(PKG):=$(LIBDIR)/libskymixer.a
ALLOBJ:=skymixer3_module.o almmixer_module.o pntsrcmixer.o
ALLOBJ:=$(ALLOBJ:%=$(OD)/%)
OBJ_$(PKG):=$(ALLOBJ)

ODEP:=$(HDR_$(PKG)) $(HDR_Healpix_cxx) $(HDR_cxxsupport) $(HDR_libsharp) $(HDR_libfftpack) $(HDR_c_utils)

$(LIB_$(PKG)): $(ALLOBJ)

$(ALLOBJ): $(ODEP) | $(OD)_mkdir
$(OD)/skymixer3_module.o: $(SD)/spectra.cc $(SD)/detector_responses.cc $(SD)/emitters.cc

all_hdr+=$(HDR_$(PKG))
all_lib+=$(LIB_$(PKG))
