PKG:=multimod

SD:=$(SRCROOT)/$(PKG)
OD:=$(BLDROOT)/$(PKG)

HDR_$(PKG):=$(SD)/*.h
LIB_$(PKG):=$(LIBDIR)/libmultimod.a
ALLOBJ:=writers.o multimod_module.o gapmaker.o
ALLOBJ:=$(ALLOBJ:%=$(OD)/%)
OBJ_$(PKG):=$(ALLOBJ)

ODEP:=$(HDR_$(PKG)) $(HDR_pntsrcconvolver) $(HDR_cxxmod) $(HDR_Healpix_cxx) $(HDR_cxxsupport) $(HDR_libsharp) $(HDR_libfftpack) $(HDR_c_utils)
$(LIB_$(PKG)): $(ALLOBJ)

$(ALLOBJ): $(ODEP) | $(OD)_mkdir

all_hdr+=$(HDR_$(PKG))
all_lib+=$(LIB_$(PKG))
