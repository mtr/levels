PKG:=cxxtools

SD:=$(SRCROOT)/$(PKG)
OD:=$(BLDROOT)/$(PKG)

LIB_$(PKG):=$(LIBDIR)/libcxxtools.a
ALLOBJ:= addMaps.o makeMap.o HPXconvert_cxx.o rotmap_cxx.o dmc_transfer.o beamsampler.o pntsrc2alm.o addTOI.o fpdbhelper.o focalplane.o ddlconvert.o sat2quat.o pointing_errors.o ringset2map.o pntsrc2map.o read_horizons.o object_inspector.o ephemeris_helper.o text2powspec.o rotalm_phi.o earl_importer.o ringset2mapset.o sat_convert.o
ALLOBJ:=$(ALLOBJ:%=$(OD)/%)
OBJ_$(PKG):=$(ALLOBJ)

ODEP:=$(HDR_pntsrcconvolver) $(HDR_cxxmod) $(HDR_Healpix_cxx) $(HDR_cxxsupport) $(HDR_libsharp) $(HDR_libfftpack) $(HDR_c_utils)

$(ALLOBJ): $(ODEP) | $(OD)_mkdir
$(LIB_$(PKG)): $(ALLOBJ)

all_lib+=$(LIB_$(PKG))
