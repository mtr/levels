#include "morton_utils.h"
#include "c_utils.h"

static int64_t t00(void)
  {
  int64_t cnt=0;
  for (uint32_t x=0; x<0x400; ++x)
    for (uint32_t y=0; y<0x400; ++y)
      for (uint32_t z=0; z<0x400; ++z)
        {
        ++cnt;
        uint32_t x1,y1,z1;
        morton2coord3D_32(coord2morton3D_32(x,y,z),&x1,&y1,&z1);
        if ((x!=x1)||(y!=y1)||(z!=z1))
          return -1;
        }
  return cnt;
  }
static int64_t t01(void)
  {
  int64_t cnt=0;
  for (uint32_t x=0; x<0x400; ++x)
    for (uint32_t y=0; y<0x400; ++y)
      for (uint32_t z=0; z<0x400; ++z)
        {
        ++cnt;
        uint32_t x1,y1,z1;
        block2coord3D_32(coord2block3D_32(x,y,z),&x1,&y1,&z1);
        if ((x!=x1)||(y!=y1)||(z!=z1))
          return -1;
        }
  return cnt;
  }
static int64_t t02(void)
  {
  int64_t cnt=0;
  for (uint32_t v=0; v<0x40000000; v+=15,++cnt)
    if (v!=peano2morton3D_32(morton2peano3D_32(v,10),10))
      return -1;
  return cnt;
  }
static int64_t t03(void)
  {
  for (uint32_t v=0; v<0x40000000; ++v)
    if (v!=block2morton3D_32(morton2block3D_32(v)))
      return -1;
  return 0x40000000;
  }
static int64_t t04(void)
  {

  int64_t cnt=0;
  for (uint32_t v=0; v<0xffffffe0; v+=15,++cnt)
    if (v!=peano2morton2D_32(morton2peano2D_32(v,16),16))
      return -1;
  return cnt;
  }
static int64_t t10(void)
  {
  int64_t cnt=0;
  for (uint32_t x=0; x<0x10000; ++x)
    for (uint32_t y=0; y<0x10000; ++y)
      {
      ++cnt;
      uint32_t x1,y1;
      morton2coord2D_32(coord2morton2D_32(x,y),&x1,&y1);
      if ((x!=x1)||(y!=y1))
        return -1;
      }
  return cnt;
  }
static int64_t t11(void)
  {
  int64_t cnt=0;
  for (uint32_t x=0; x<0x10000; ++x)
    for (uint32_t y=0; y<0x10000; ++y)
      {
      ++cnt;
      uint32_t x1,y1;
      block2coord2D_32(coord2block2D_32(x,y),&x1,&y1);
      if ((x!=x1)||(y!=y1))
        return -1;
      }
  return cnt;
  }
static int64_t t12(void)
  {
  uint32_t v=0;
  do
    if (v!=block2morton2D_32(morton2block2D_32(v)))
      return -1;
  while (++v!=0);
  return 0x100000000;
  }
static int64_t t20(void)
  {
  int64_t cnt=0;
  for (uint64_t x=0; x<0xffff0000; x+=0xfff5)
    for (uint64_t y=0; y<0xffff0000; y+=0xfff7)
      {
      ++cnt;
      uint64_t x1,y1;
      morton2coord2D_64(coord2morton2D_64(x,y),&x1,&y1);
      if ((x!=x1)||(y!=y1))
        return -1;
      }
  return cnt;
  }
static int64_t t21(void)
  {
  int64_t cnt=0;
  for (uint64_t x=0; x<0xffff0000; x+=0xfff5)
    for (uint64_t y=0; y<0xffff0000; y+=0xfff7)
      {
      ++cnt;
      uint64_t x1,y1;
      block2coord2D_64(coord2block2D_64(x,y),&x1,&y1);
      if ((x!=x1)||(y!=y1))
        return -1;
      }
  return cnt;
  }
static int64_t t22(void)
  {
  int64_t cnt=0;
  for (uint64_t v=0; v<0xffffffff00000000; v+=0xfffff563, ++cnt)
    if (v!=block2morton2D_64(morton2block2D_64(v)))
      return -1;
  return cnt;
  }
static int64_t t30(void)
  {
  int64_t cnt=0;
  for (uint64_t x=0; x<0x200000; x+=0xff34)
    for (uint64_t y=0; y<0x200000; y+=0xff84)
      for (uint64_t z=0; z<0x200000; z+=0xff96)
        {
        ++cnt;
        uint64_t x1,y1,z1;
        morton2coord3D_64(coord2morton3D_64(x,y,z),&x1,&y1,&z1);
        if ((x!=x1)||(y!=y1)||(z!=z1))
          return -1;
        }
  return cnt;
  }
static int64_t t31(void)
  {
  int64_t cnt=0;
  for (uint32_t x=0; x<0x400; ++x)
    for (uint32_t y=0; y<0x400; ++y)
      for (uint32_t z=0; z<0x400; ++z)
        {
        ++cnt;
        uint32_t x1,y1,z1;
        block2coord3D_32(coord2block3D_32(x,y,z),&x1,&y1,&z1);
        if ((x!=x1)||(y!=y1)||(z!=z1))
          return -1;
        }
  return cnt;
  }
static int64_t t32(void)
  {
  int64_t cnt=0;
  for (uint64_t v=0; v<0x7fffffff00000000; v+=0xfffffff78, ++cnt)
    if (v!=peano2morton3D_64(morton2peano3D_64(v,21),21))
      return -1;
  return cnt;
  }
static int64_t t33(void)
  {
  int64_t cnt=0;
  for (uint64_t v=0; v<0x7fffffff00000000; v+=0xffffff78, ++cnt)
    if (v!=block2morton3D_64(morton2block3D_64(v)))
      return -1;
  return cnt;
  }
static int64_t t34(void)
  {
  int64_t cnt=0;
  for (uint64_t v=0; v<0xffffffff00000000; v+=0xfffffff78, ++cnt)
    if (v!=peano2morton2D_64(morton2peano2D_64(v,32),32))
      return -1;
  return cnt;
  }

#include <stdio.h>
#include "walltime_c.h"

typedef int64_t (*testfunc)(void);

static void runtest(testfunc tf, const char *tn)
  {
  double t=wallTime();
  int64_t res=tf();
  if (res<0)
    printf("%s FAILED\n",tn);
  else
    printf("%s OK. MOps/s: %7.2f\n",tn,2e-6*res/(wallTime()-t));
  }

int morton_test(int argc, const char **argv);
int morton_test(int argc, const char **argv)
  {
  UTIL_ASSERT((argc==1)||(argv[0]==NULL),"problem with args");
  runtest(t10,"coord  <-> Morton 2D 32bit");
  runtest(t11,"coord  <-> Block  2D 32bit");
  runtest(t12,"Morton <-> Block  2D 32bit");
  runtest(t04,"Morton <-> Peano  2D 32bit");
  runtest(t20,"coord  <-> Morton 2D 64bit");
  runtest(t21,"coord  <-> Block  2D 64bit");
  runtest(t22,"Morton <-> Block  2D 64bit");
  runtest(t34,"Morton <-> Peano  2D 64bit");
  runtest(t00,"coord  <-> Morton 3D 32bit");
  runtest(t01,"coord  <-> Block  3D 32bit");
  runtest(t02,"Morton <-> Peano  3D 32bit");
  runtest(t03,"Morton <-> Block  3D 32bit");
  runtest(t30,"coord  <-> Morton 3D 64bit");
  runtest(t31,"coord  <-> Block  3D 64bit");
  runtest(t32,"Morton <-> Peano  3D 64bit");
  runtest(t33,"Morton <-> Block  3D 64bit");
  return 0;
  }
