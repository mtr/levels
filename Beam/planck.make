PKG:=Beam

SD:=$(SRCROOT)/$(PKG)
OD:=$(BLDROOT)/$(PKG)

FULL_INCLUDE_F+= $(F_MODPATH)$(OD)

LIB_$(PKG):=$(LIBDIR)/libbeam.a
ALLOBJ:=beam_grid.o beam_cut.o beam_polar.o beam_square.o beam_bessel.o beam_alm.o beam_convert.o beam_interpolate.o beam_crosspol.o beam_transform.o beam.o beam2alm_main.o grasp2stokes_main.o crosspol_main.o gaussbeampol_main.o stokes_extract.o
ALLOBJ:=$(ALLOBJ:%=$(OD)/%)
OBJ_$(PKG):=$(ALLOBJ)
MODULES:=beam2alm_main grasp2stokes_main crosspol_main gaussbeampol_main stokes_extract_main
MODULES:=$(MODULES:%=$(OD)/%.$(MOD))

ODEP:=$(OBJ_Modules) $(OBJ_libsharp_f) $(OBJ_portability)
$(LIB_$(PKG)): $(ALLOBJ)
$(ALLOBJ): $(ODEP) | $(OD)_mkdir
f90prep: | $(OD)_mkdir

$(OD)/beam_alm.o: $(OD)/beam_bessel.o
$(OD)/beam_convert.o: $(OD)/beam_grid.o $(OD)/beam_cut.o $(OD)/beam_polar.o $(OD)/beam_square.o
$(OD)/beam_interpolate.o: $(OD)/beam_polar.o $(OD)/beam_square.o
$(OD)/beam_crosspol.o: $(OD)/beam_polar.o $(OD)/beam_square.o
$(OD)/beam_transform.o: $(OD)/beam_polar.o $(OD)/beam_alm.o
$(OD)/beam.o: $(OD)/beam_grid.o $(OD)/beam_cut.o $(OD)/beam_polar.o $(OD)/beam_square.o $(OD)/beam_bessel.o $(OD)/beam_alm.o $(OD)/beam_convert.o $(OD)/beam_interpolate.o $(OD)/beam_crosspol.o $(OD)/beam_transform.o
$(OD)/grasp2stokes_main.o $(OD)/beam2alm_main.o $(OD)/crosspol_main.o $(OD)/gaussbeampol_main.o $(OD)/stokes_extract.o: $(OD)/beam.o

all_lib+=$(LIB_$(PKG))
all_mod+=$(MODULES)
