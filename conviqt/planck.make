PKG:=conviqt

SD:=$(SRCROOT)/$(PKG)
OD:=$(BLDROOT)/$(PKG)

LIB_$(PKG):=$(LIBDIR)/libconviqt_v3.a
ALLOBJ:=conviqt_v3_module.o conviqt_v4_module.o ringset_compare.o
ALLOBJ:=$(ALLOBJ:%=$(OD)/%)
OBJ_$(PKG):=$(ALLOBJ)

ODEP:=$(HDR_Healpix_cxx) $(HDR_cxxsupport) $(HDR_libsharp) $(HDR_libfftpack) $(HDR_c_utils)

$(LIB_$(PKG)): $(ALLOBJ)

$(ALLOBJ): $(ODEP) | $(OD)_mkdir

all_lib+=$(LIB_$(PKG))
